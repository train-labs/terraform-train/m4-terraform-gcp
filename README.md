# 0) Normalizar estructura
- 1. Parametrizar modulo "terraform-gcp-compute-engine".

# 1) Validar cambios - Validate

Comando valida los archivos de configuración en un directorio, refiriéndose solo a la configuración y sin acceder a ningún estado remoto

> $ terraform validate

## A) Testing estatico - Lint
https://github.com/terraform-linters/tflint

```
$ curl -s https://raw.githubusercontent.com/terraform-linters/tflint/master/install_linux.sh | bash

$ tflint --init
```
## B) Testing estático - Checkov

https://github.com/bridgecrewio/checkov

## Instalar docker
```
$ sudo apt-get update
$ sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
  echo \
   "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
 
$ sudo apt-get update
$ sudo apt-get install docker-ce docker-ce-cli containerd.io
```
## Ejecutar análisis
```
$ docker pull bridgecrew/checkov
$ docker run --tty --volume /home/USER_NAME/labs-terraform-gcp:/tf bridgecrew/checkov --directory /tf

```

# 2) Planificar ejecución - Plan

Crea un plan de ejecución sobre las configuraciones de recursos. Internamente Terraform realiza:

- Lectura del estado remoto de los recursos.
- Compara el estado actual con el anterior y observa las diferencias.

---

**-out**

Puede usar la opción -out=PLAN_FILE (opcional) para guardar el plan generado en un archivo, que luego puede ejecutar pasando el archivo como ```'terraform apply PLAN_FILE'```

> $ terraform plan  -out=PLAN_FILE


---
**Destroy**

Crea un plan cuyo objetivo es destruir todos los objetos remotos que existen actualmente

> $ terraform plan -destroy


# 3) Aplicar cambios de infraestructura - Apply

Este comando ejecuta los manifiestos propuestos para el aprovicionamiento de recursos.

la forma basica de aplicar este comando es ```terraform apply```, el cual genera un plan de ejecución y solicita la confirmación; Sin embargo puede ser utilizado con algunas opciones según sea el caso de uso.

---
**-auto-approve**

Terraform realiza su propio plan y acepta los cambios.
> $ terraform apply -auto-approve

---
**PLAN_FILE**

Es posible ejecutar un plan previamente almacenado sin cofirmación alguna.
> $ terraform apply [PLAN_FILE]

---


# 4) Visualizar recursos - Graph

Se utiliza para generar una representación visual de una configuración o un plan de ejecución. La salida está en formato DOT, que GraphViz puede utilizar para generar gráficos.

```
$ apt-get install graphviz

$ terraform graph | dot -Tsvg > graph.svg
```
Tipos de graficos como salida, pueden ser: ```plan```, ```plan-destroy```, ```apply```, ```validate```, ```input```, ```refresh```.

```
$ terraform graph -type=plan-destroy | dot -Tsvg > graph.svg
```


# 5) Destruir recursos - Destroy

Comando es una forma conveniente de destruir todos los objetos remotos administrados por una configuración particular de Terraform.

```
$ terraform destroy
`````