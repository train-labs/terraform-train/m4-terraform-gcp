## GLOBAL VALUES
project          = "<PROJECT_ID>"
credentials_file = "<FILE>"
region           = "us-central1"
zone             = "us-central1-c"
## VPC VALUES
net_name     ="<NETWORK_NAME>"

## INSTANCE VALUES
instance_name   = "<COMPUTE_NAME>"
instance_type   = "f1-micro"
instance_image  = "debian-cloud/debian-9"